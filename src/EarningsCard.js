import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

const EarningsCard = ()=> {
    return (
      <View style={styles.mainContainer}>
        <Text style={styles.textStyle}> Amount </Text>
      </View>
    )
}

const styles = StyleSheet.create({
    mainContainer:{
        height: 100,
        backgroundColor: 'white',  
        justifyContent: 'center',
        alignItems: 'center'
    },

    textStyle:{
        fontSize: 30,
        fontFamily: 'OpenSans-Bold',
        color: 'black'
    }

})

export default EarningsCard
