import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native'

 const DetailCard = ({clientname, phone, modelNo, sType, amount, status,id,onPress})=> {
    return (
      <View style={styles.mainContainer}>
        <View style={{flex:1}}>
           <View style={{flex:1, justifyContent: "flex-end", alignItems: 'center'}}>
                <Text style={styles.normalText}>ID</Text>
           </View>
           <View style={{flex:2, justifyContent: "center", alignItems: 'center'}}>
                <Text style={styles.probedText}>{id}</Text>
           </View>
        </View>
        <View style={{flex:1}}>
           <View style={{flex:1, justifyContent: "flex-end", alignItems: 'center'}}>
                <Text style={styles.normalText}>Name</Text>
           </View>
           <View style={{flex:2, justifyContent: "center", alignItems: 'center'}}>
                <Text style={styles.probedText}>{clientname}</Text>
           </View>
        </View>
        <View style={{flex:1}}>
           <View style={{flex:1, justifyContent: "flex-end", alignItems: 'center'}}>
                <Text style={styles.normalText}>Phone</Text>
           </View>
           <View style={{flex:2, justifyContent: "center", alignItems: 'center'}}>
                <Text style={styles.probedText}>{phone}</Text>
           </View>
        </View>
        {/* <View style={{flex:1}}>
           <View style={{flex:1, justifyContent: "flex-end", alignItems: 'center'}}>
                <Text style={styles.normalText}>Place</Text>
           </View>
           <View style={{flex:2, justifyContent: "center", alignItems: 'center'}}>
                <Text style={styles.probedText}>Probed Place</Text>
           </View>
        </View> */}
        <View style={{flex:1}}>
           <View style={{flex:1, justifyContent: "flex-end", alignItems: 'center'}}>
                <Text style={styles.normalText}>Model No</Text>
           </View>
           <View style={{flex:2, justifyContent: "center", alignItems: 'center'}}>
                <Text style={styles.probedText}>{modelNo}</Text>
           </View>
        </View>
        <View style={{flex:1}}>
           <View style={{flex:1, justifyContent: "flex-end", alignItems: 'center'}}>
                <Text style={styles.normalText}>Service Type</Text>
           </View>
           <View style={{flex:2, justifyContent: "center", alignItems: 'center'}}>
                <Text style={styles.probedText}>{sType}</Text>
           </View>
        </View>
        <View style={{flex:1}}>
           <View style={{flex:1, justifyContent: "flex-end", alignItems: 'center'}}>
                <Text style={styles.normalText}>Amount</Text>
           </View>
           <View style={{flex:2, justifyContent: "center", alignItems: 'center'}}>
                <Text style={styles.probedText}>{amount}</Text>
           </View>
        </View>
        <View style={{flex:1}}>
           <View style={{flex:1, justifyContent: "flex-end", alignItems: 'center'}}>
                <Text style={styles.normalText}>Status</Text>
           </View>
           <View style={{flex:2,}}>
                <TouchableOpacity style={{flex:1, borderWidth:2, margin: 15, justifyContent: "center", alignItems:'center'}} onPress={onPress}>
                <Text style={[styles.probedText]}>{status}</Text>
                </TouchableOpacity>
           </View>
        </View>
      </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        height: 90,
        backgroundColor: 'white',
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginHorizontal: 5
    },
    probedText:{
        fontSize: 12,
        fontFamily: "OpenSans-Bold",
        color: 'black'
    },
    normalText:{
        fontSize: 9,
        fontFamily: "OpenSans-SemiBold",
        color: 'grey'

    }
})
export default DetailCard