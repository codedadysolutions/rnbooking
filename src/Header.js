import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/dist/Entypo'

const Header = ({title, onPresss})=> {
    return (
      <View style={styles.mainContainer}>
       <TouchableOpacity onPress = {onPresss} style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
        <Icon name="menu" size={30} color="white" />
       </TouchableOpacity>
       <View style={{flex:9, justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 20}}>
           <Text style={styles.headLineText}>{title}</Text>
       </View>
       {/* <View>
        <Icon name="rocket" size={30} color="white" />
       </View> */}
      </View>
    )
}

const styles = StyleSheet.create({
    mainContainer:{
        height: 60,
        flexDirection: 'row',
        borderColor: 'white',
        borderBottomWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headLineText: {
        fontSize: 20,
        fontFamily: 'OpenSans-Bold',
        color: 'white'

    }
})

export default Header
