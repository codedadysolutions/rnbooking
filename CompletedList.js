import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView,FlatList,ActivityIndicator,TouchableOpacity,Alert } from 'react-native'
import DetailCard from './src/DetailCard'
import Header from './src/Header'
import firebase from 'react-native-firebase'
import moment from 'moment'
import {theme} from './config'

export default class CompletedList extends Component {

    state= {
        clientname: "Salih", 
        phone: "9746169898", 
        vehicleNo: "KL 53 J 9666", 
        sType: 'Full', 
        amount: '300', 
        status: 'Paid',
        data:[],
        refresh:false,
        loader:true
    }

    componentDidMount(){
      var db = firebase.firestore();
      var that =this
  
      db.collection("users").where("status", "==", "completed")
      .get()
      .then(function(querySnapshot) {
          const data = [];
      
          querySnapshot.forEach(function(doc) {
              data.push({
                  key: doc.id,
                  name:doc.data().name,
                  phone:doc.data().phone,
                  place:doc.data().place,
                  type:doc.data().type,
                  modelnumber:doc.data().modelnumber,
                  amount:doc.data().amount
                });
          });
         
          that.setState({
              data,
              loader:false
          })
      })
      .catch(function(error) {
        that.setState({
            loader:false
        })
          console.log("Error getting documents: ", error);
      });
      
  }

  onLongPressedReturn=(key)=>{
    Alert.alert(
        'Alert',
        'Are you sure want to back this entry',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => this.changefromDb(key)},
        ],
        { cancelable: false }
      )
}


changefromDb =async(key)=>{

    var data =this.state.data
    var removeByAttr = function(arr, attr, value){
        var i = arr.length;
        while(i--){
           if( arr[i] 
               && arr[i].hasOwnProperty(attr) 
               && (arguments.length > 2 && arr[i][attr] === value ) ){ 
    
               arr.splice(i,1);
    
           }
        }
        return arr;
    }
    
        var list = removeByAttr(data, 'key', key);
        this.setState({data:list,refresh: !this.state.refresh})

        var db = firebase.firestore();
        db.collection("users").doc(key).update({
            status:'working',
        });



}

  onPress = async(key)=>{

    var data =this.state.data
    var removeByAttr = function(arr, attr, value){
        var i = arr.length;
        while(i--){
           if( arr[i] 
               && arr[i].hasOwnProperty(attr) 
               && (arguments.length > 2 && arr[i][attr] === value ) ){ 
    
               arr.splice(i,1);
    
           }
        }
        return arr;
    }
    
        var list = removeByAttr(data, 'key', key);
        this.setState({data:list,refresh: !this.state.refresh})
    
        var db = firebase.firestore();
        db.collection("users").doc(key).update({
            status:'paid',
            dateModified:moment().format('DD/MM/YYYY'),
            month:moment().format('MMMM YYYY')
        });
    
    
    }

  renderCard =()=>{
    if(this.state.loader){
        return(
            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                <ActivityIndicator size={42} color="white"/>
            </View>
        )
    }else{
    return(
        <FlatList
        extraData={this.state.refresh}
        data={this.state.data}
        renderItem={({ item,index }) => (
            <TouchableOpacity onLongPress={()=>this.onLongPressedReturn(item.key)}>
                <DetailCard id={index + 1} clientname = {item.name} phone = {item.phone} modelNo = {item.modelnumber} sType = {item.type} amount = {item.amount} status = {this.state.status} onPress={()=>this.onPress(item.key)}/>
            </TouchableOpacity>
        )
        }
    />
    )
    }
}
  
  render() {
    return (
      <View style={styles.mainContainer}>
        <Header title = "Completed List" onPresss = {()=>this.props.navigation.openDrawer()}/>
    
          {this.renderCard()}
    
      </View>
    )
  }
}

const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
        backgroundColor: theme
    },
})
