import React, { Component } from 'react'
import { createStackNavigator } from 'react-navigation'
import Home from './App'
import Login from './Login'
import Splashscreen from './Splashscreen'

const RootStack = createStackNavigator({
  Splashscreen:{
    screen:Splashscreen
    },
    Login: {
      screen: Login
    },
    Home: {
      screen: Home
    }  
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
   }
  );
  
  export default class Router extends React.Component {
    render() {
      return <RootStack />
    }
  }
