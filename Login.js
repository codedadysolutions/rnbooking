import React, { Component } from 'react'
import { Text, StyleSheet, View,TouchableOpacity,Alert, ActivityIndicator, ScrollView } from 'react-native'
import firebase from 'react-native-firebase'
import { TextField } from 'react-native-material-textfield'
import Header from './src/Header'
import {loginemail, loginpassword, theme} from './config'

export default class Login extends Component {
  state ={
    email:loginemail,
    password:loginpassword,
    errMail:'',
    errPass:'',
    loader:false,
    messageCount:'34'
  }


  async componentDidMount () {
    var that = this
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        that.props.navigation.replace('Home',{ count: 23 })
      }
    });
  }

  checkMessageLimit =()=>{
    var db = firebase.firestore()
    var that = this

            db.collection("loginAcess").get().then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                  if(doc.data().messageCount <= 0){
                    Alert.alert(
                      'Warning',
                      'Message Limit exceeds pls contact +918606550666',
                      [
                        
                      ],
                      { cancelable: false }
                    )
                  }else{
                    // that.setState({messageCount:doc.data().messageCount})
                    console.warn(doc.data().messageCount)
                  }
            });
        });
  }
  
  onSubmit = ()=>{
    this.setState({loader:true})
     if (this.state.email == '' ){
       this.setState({errMail: 'Please enter the email',loader:false})
     }
     else if (this.state.password == ''){
       this.setState({errPass: 'Please enter the password',loader:false})
     }
     else{
        this.loginUser()
      
     }
 }

  loginUser =()=>{
    var db = firebase.firestore()
    var that = this

            db.collection("loginAcess").get().then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                  if(doc.data().limit <= doc.data().count){
                    that.setState({loader:false})
                    Alert.alert(
                      'Warning',
                      'Login Limit excessds',
                      [
                        
                      ],
                      { cancelable: false }
                    )
                  }else{
                    that.loginSuccess()
                  }
            });
        });

   
  }

  loginSuccess =()=>{
    var that = this
    firebase.auth().signInWithEmailAndPassword(this.state.email,this.state.password).then((data)=>{
         that.addUserCount()
        })
        .catch(function(error) {
          console.warn(error)
          that.setState({loader: false})
        });
  
  }

  addUserCount =()=>{
    var db = firebase.firestore();
    var sfDocRef = db.collection("loginAcess").doc('detail')
    var that = this

    return db.runTransaction(function(transaction) {
      
        return transaction.get(sfDocRef).then(function(sfDoc) {
            if (!sfDoc.exists) {
                throw "Document does not exist!";
            }
              var newCount = sfDoc.data().count + 1;
              transaction.update(sfDocRef, { count: newCount });
          });

      }).then(function() {
     
        that.props.navigation.replace('Home')

      }).catch(function(error) {
          console.log("Transaction failed: ", error);
      });
  }

  renderLoader =()=>{
    if(this.state.loader){
        return (
          <View style={styles.buttonView}>
              <ActivityIndicator color="black" size={36} />
           </View>
        )
    }else{
        return(
        <TouchableOpacity onPress = {this.onSubmit} style={styles.buttonView}>
             <Text style={styles.buttonText}>Login</Text>
         </TouchableOpacity>
        )
    }
}
  


  render() {

    let { email, password } = this.state;

    return (
      
        // {/* <TouchableOpacity onPress={()=>this.loginUser()}>
        //   <Text style={{fontSize:22}}> Login </Text>
        // </TouchableOpacity> */}
        <View style={styles.mainContainer}>
        <Header title = 'Login'/>
      <ScrollView styles={{flex:1}}>
            {/* <View style={styles.registerView}>
                <Text style={styles.headLineText}>Register</Text>
            </View> */}

            <View style={styles.textFieldView}>
                        <TextField
                        label='Email'
                        value={email}
                        textColor='white'
                        tintColor= 'white'
                        baseColor= 'white'
                        onChangeText={ (email) => this.setState({ email, errMail: '' }) }
                        error = {this.state.errMail}
                    />

            </View>
            <View style={styles.textFieldView}>
                <TextField
                            label='Password'
                            value={password}
                            // prefix= '+91'
                            textColor='white'
                            tintColor= 'white'
                            baseColor= 'white'
                            secureTextEntry={true}
                            onChangeText={ (password) => this.setState({ password, errPass:'' }) }
                            error = {this.state.errPass}
                        />
            </View>

        </ScrollView>
        {this.renderLoader()}
        </View>
          
    )
  }
}

const styles = StyleSheet.create({
  mainContainer:{
      flex:1,
      backgroundColor: theme
  },
  registerView:{
      height: 70,
      borderWidth: 0,
      justifyContent: 'center',
      alignItems: 'center',

  },
  textFieldView:{
      height: 50,
      borderWidth: 0,
      margin: 20,

  },
  buttonView:{
      // flex:.5,
      height: 60,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 0,
      marginTop: 30
  },
  headLineText:{
     fontSize: 22,
     fontFamily: 'OpenSans-Bold',
     color: 'white'
  },
  buttonText:{
      fontSize: 17,
      color: 'black',
      fontFamily: 'OpenSans-Bold'
  },
  iconView:{

  }
})
