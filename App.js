import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,SafeAreaView,ScrollView, Image,Alert,TouchableOpacity} from 'react-native';
import {createDrawerNavigator,DrawerItems} from 'react-navigation'
import Register from './Register'
import WorkingList from './WorkingList'
import CompletedList from './CompletedList'
import Database from './Database'
import Earnings from './Earnings'
import {Appname,test} from './config'
import firebase from 'react-native-firebase'
import CustomDrawerComponent from './customDrawer'
import OfflineNotice from './src/offlineNotice'


export default class App extends Component {
  state ={
    messageCount:'23'
  }

  async componentDidMount(){
   
    var that = this
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        
      } else {
        that.props.navigation.navigate('Login')
      }
    });


    // this.checkMessageLimit()
  }


 

  render() {
    return (
      <View style={{flex:1}}>
      <OfflineNotice />
     <AppDrawerNavigator />
     </View>
    );
  }
}






const AppDrawerNavigator =createDrawerNavigator({
  WorkingList: WorkingList,
  Register:Register,
  CompletedList: CompletedList,
  Database: Database,
  Earnings: Earnings
  
},{
   contentComponent:CustomDrawerComponent
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },

 


});