const Appname = "Codedady"
const mindate = "25-12-2018"   //Purchase Date DD-MM-YYYY
const minEarning ="2018-12-25" //Purchase Date YYYY-MM-DD
const theme = "#101839"  //default #101839 

const bussinessname = "Codedady"
const loginemail="test@gmail.com" //for auto seting in login screen
const loginpassword="test123"     //for auto seting in login screen

const bussinesspic =""
const senderid="CODEDY" //IMPORTANT => 6 Capital Letters MUST 
const bussinessphone="+918606550666" //Business Phone Number


const messageAPI = '253089Ap2dXKdOp9Z5c1e4155'
const serviceTypology ='Service' //for Completed SMS
const serviceTypologyR ='Mobile Repair' //for Register SMS


const dataService = [  //Add data for Service types using Same format

   {
       id:"1",
      name:"Dead",
      price:300

   },
   {
       id:"2",
      name:"Display",
      price:200

   },
   {
       id:"3",
       name:"Software",
       price:500

   },
  


]

export {
   Appname,
   mindate,
   bussinessname,
   loginemail,
   loginpassword,
   bussinesspic,
   senderid,
   bussinessphone,
   minEarning,
   messageAPI,
   serviceTypology,
   serviceTypologyR,
   dataService, 
   theme
}

