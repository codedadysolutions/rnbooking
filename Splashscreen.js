import React, { Component } from 'react'
import { Text, StyleSheet, View,Image } from 'react-native'
import {serviceTypologyR} from './config'

export default class Splashscreen extends Component {
    componentDidMount(){
        setTimeout(() => {this.props.navigation.replace('Login')}, 3000)
    }
  render() {
    return (
      <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
        <Image source={require('./assets/images/mobileService.png')} style={{width:464/3,height:410/3, marginBottom:60}} />
        <Text style={{fontSize:25, fontFamily:'OpenSans-Bold', color:'black'}}> {serviceTypologyR} </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({})
