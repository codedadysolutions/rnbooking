import React, { Component } from 'react'
import { Text, StyleSheet, View,SafeAreaView,Image,ScrollView,TouchableOpacity,Alert,Linking} from 'react-native'
import { NavigationActions,DrawerItems } from 'react-navigation'
import firebase from 'react-native-firebase'
import {bussinessname} from './config'
export default class customDrawer extends Component {

    state ={
        messageCount:''
    }

    componentDidMount(){
        var db = firebase.firestore()
        // this.checkMessageLimit()
        var that = this
        db.collection("loginAcess").doc("detail")
    .onSnapshot(function(doc) {
        
        if(doc.data().messageCount <= 0){
            Alert.alert(
              'Warning',
              'Message Limit exceeds pls contact +918606550666',
              [
                // {text: 'OK', onPress: () => this.pressCall},
              ],
              { cancelable: false }
            )
          }else{
            that.setState({messageCount:doc.data().messageCount})
        //    console.warn(doc.data())
          }
    });
    }

    pressCall(){
        const url='tel://+918606550666'
        Linking.openURL(url)
    }

    // checkMessageLimit =()=>{
    //     var db = firebase.firestore()
    //     var that = this
    
    //             db.collection("loginAcess").get().then(function(querySnapshot) {
    //             querySnapshot.forEach(function(doc) {
                     
    //             });
    //         });
    //   }
  render() {
    return (
        <SafeAreaView style={{flex:1}} >
        <View style={{height:120,width:300, flexDirection: 'row'}}>
            {/* <Text>hellooo,working</Text> */}
            <View style={{flex:1, justifyContent: 'center', alignItems: 'center', borderWidth: 0}}>
                <Image source={require('./assets/Company/user.png')} style={{height: 80, width: 80, borderRadius: 40}}/>
            </View>
            <View style={{flex:2,justifyContent: 'center', alignItems: 'flex-start', borderWidth: 0, borderRadius: 40}}>
                <Text style={styles.companyText}>{bussinessname}</Text>
                <Text style={{paddingLeft:10, fontFamily:"OpenSans-SemiBold", fontSize: 12, marginTop: 10}}>Balance message :{this.state.messageCount}  </Text>
            </View>
        </View>
        <ScrollView>
          <DrawerItems {...this.props} />
        </ScrollView>
       <TouchableOpacity onPress={async()=>{
            firebase.auth().signOut()
            .then(function() {
              var db = firebase.firestore();
              var sfDocRef = db.collection("loginAcess").doc('detail')
              var that = this
          
              return db.runTransaction(function(transaction) {
                
                  return transaction.get(sfDocRef).then(function(sfDoc) {
                      if (!sfDoc.exists) {
                          throw "Document does not exist!";
                      }
                        var newCount = sfDoc.data().count - 1;
                        transaction.update(sfDocRef, { count: newCount });
                    });
          
                }).then(function() {
                    
                }).catch(function(error) {
                    console.log("Transaction failed: ", error);
                });
    
            })
            .catch(function(error) {
              // An error happened
            });
       }} style={{alignItems:'center',borderColor:'black',borderRadius:8,borderWidth:2,height:50,justifyContent:'center',marginBottom:15,marginHorizontal:15}}>
          <Text style={styles.logoutText}>Logout</Text>
       </TouchableOpacity> 
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
    companyText:{
        fontSize: 18,
        fontFamily: 'OpenSans-Bold',
        paddingLeft: 10
      },
      logoutText:{
        fontSize: 15,
        fontFamily: 'OpenSans-Bold',
        color: "black"
      }
})
