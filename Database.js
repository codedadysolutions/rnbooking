import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, TouchableOpacity,FlatList,ActivityIndicator } from 'react-native'
import Header from './src/Header'
import DetailCard from './src/DetailCard'
import Icon from 'react-native-vector-icons/dist/Entypo'
import firebase from 'react-native-firebase'
import moment from 'moment'
import DatePicker from 'react-native-datepicker'
import Modal from "react-native-simple-modal";
import  {mindate, theme} from './config'

export default class Database extends Component {

    state={
        todayTotalamount: 0,
        clientname: "Salih", 
        phone: "9746169898", 
        vehicleNo: "KL 53 J 9666", 
        sType: 'Full', 
        amount: 300, 
        status: 'Completed',
        data:[],
        date:'',
        open:false,
        dateModified: false,
        loader:true
    }
componentDidMount(){
    var db = firebase.firestore();
    var that =this
    let date = new Date()

    this.setState({date:moment(date).format('DD/MM/YYYY')})

    db.collection("users").where('dateModified', '==',moment().format('DD/MM/YYYY')).where("status", "==", "paid")
    .get()
    .then(function(querySnapshot) {
        const data = [];
        var amount = that.state.todayTotalamount

        querySnapshot.forEach(function(doc) {
            data.push({
                key: doc.id,
                name:doc.data().name,
                phone:doc.data().phone,
                place:doc.data().place,
                type:doc.data().type,
                modelnumber:doc.data().modelnumber,
                amount:doc.data().amount
              });
              amount = amount + doc.data().amount
        });
        that.setState({
            data,
            todayTotalamount:amount,
            loader:false
        })
    })
    .catch(function(error) {
        that.setState({
            loader:false
        })
        console.log("Error getting documents: ", error);
    });
}

modalDidOpen = () => console.log("Modal did open.");

modalDidClose = () => {
  this.setState({ open: false });
  console.log("Modal did close.");
};


openModal = () => this.setState({ open: true,dateModified:true });

closeModal = () => this.setState({ open: false });


pressRight =()=>{
    startdate = this.state.date
    var db = firebase.firestore();
    var that = this
    var new_date = moment(startdate, "DD/MM/YYYY").add('days',1).format('DD/MM/YYYY')
   
    this.setState({data:[],todayTotalamount:0,date:new_date})

    db.collection("users").where('dateModified', '==',new_date).where("status", "==", "paid")
    .get()
    .then(function(querySnapshot) {
        const data = [];
        var amount = that.state.todayTotalamount
    
        querySnapshot.forEach(function(doc) {
            data.push({
                key: doc.id,
                name:doc.data().name,
                phone:doc.data().phone,
                place:doc.data().place,
                type:doc.data().type,
                modelnumber:doc.data().modelnumber,
                amount:doc.data().amount
              });
              amount = amount + doc.data().amount
        });
        that.setState({
            data,
            todayTotalamount:amount,
            loader:false
        })
    })
    .catch(function(error) {
        that.setState({
            loader:false
        })
        console.log("Error getting documents: ", error);
    });
}


pressLeft =()=>{
    startdate = this.state.date
    var db = firebase.firestore();
    var that = this

    var new_date = moment(startdate, "DD/MM/YYYY").add('days',-1).format('DD/MM/YYYY')
    this.setState({data:[],todayTotalamount:0,date:new_date})

    db.collection("users").where('dateModified', '==',new_date).where("status", "==", "paid")
    .get()
    .then(function(querySnapshot) {
        const data = [];
        var amount = that.state.todayTotalamount
    
        querySnapshot.forEach(function(doc) {
            data.push({
                key: doc.id,
                name:doc.data().name,
                phone:doc.data().phone,
                place:doc.data().place,
                type:doc.data().type,
                modelnumber:doc.data().modelnumber,
                amount:doc.data().amount
              });
              amount = amount + doc.data().amount
        });
        that.setState({
            data,
            todayTotalamount:amount,
            loader:false
        })
    })
    .catch(function(error) {
        that.setState({
            loader:false
        })
        console.log("Error getting documents: ", error);
    });
}


filterDate =()=>{
    
    var db = firebase.firestore();
    var that =this
    this.setState({data:[],todayTotalamount:0})
    var date = this.state.date
    db.collection("users").where('dateModified', '==', date).where("status", "==", "paid")
    .get()
    .then(function(querySnapshot) {
        const data = [];
        var amount = that.state.todayTotalamount
    
        querySnapshot.forEach(function(doc) {
            data.push({
                key: doc.id,
                name:doc.data().name,
                phone:doc.data().phone,
                place:doc.data().place,
                type:doc.data().type,
                modelnumber:doc.data().modelnumber,
                amount:doc.data().amount
              });
             amount = amount + doc.data().amount
            
        });
        that.setState({
            data,
            todayTotalamount:amount,
            loader:false
        })
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}

renderList =()=>{

    if(this.state.loader){
        return(
            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                <ActivityIndicator size={42} color="white"/>
            </View>
        )
    }else{
    return(
        <FlatList
        data={this.state.data}
        renderItem={({ item,index }) => (
            <DetailCard id={index + 1} clientname = {item.name} phone = {item.phone} modelNo = {item.modelnumber} sType = {item.type} amount = {item.amount} status = {this.state.status} />
        )
        }
    />
    )
    }
}

checkToday =()=>{
    var today =moment().format('DD/MM/YYYY')
    var yesterday = moment(today, "DD/MM/YYYY").add('days',-1).format('DD/MM/YYYY')

    if(this.state.date === today){
        return "Today"
    }else if(this.state.date === yesterday){
        return "Yesterday"
    }else{
        return this.state.date
    }
}

  render() {
      console.warn('amount',this.state.todayTotalamount)
    return (
        <View style={styles.mainContainer}>
        <Header title = 'Database' onPresss = {()=>this.props.navigation.openDrawer()}/>
      <ScrollView style={{flex:1}}>
      {/* <View style={{height: 40, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', marginHorizontal: 10}}>
          <Text style={styles.dateText}>Today</Text>
          </View> */}
          <View style={{flexDirection: 'row', height: 70}}>
          <View style={{flex:4, backgroundColor: 'white', margin: 10, flexDirection: 'row'}}>
            <TouchableOpacity style={{flex:1,justifyContent: 'center', alignItems: 'center'}} onPress={this.pressLeft}>
                 <Icon name="arrow-left" size={30} color="black" />
            </TouchableOpacity  >
            <TouchableOpacity onPress={this.openModal} style={{flex:3, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={styles.dateText}>{this.checkToday()}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{flex:1, justifyContent: 'center', alignItems: 'center'}} onPress={this.pressRight}>
                <Icon name="arrow-right" size={30} color="black" />
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={{flex:1, backgroundColor: 'white', margin: 10, justifyContent: 'center', alignItems: 'center'}} onPress={this.openModal}>
                <Text style={styles.dateText}>Filter</Text>
          </TouchableOpacity>

          </View>

          <View style={{height: 40, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', marginHorizontal: 10}}>
          <Text style={styles.amountText}>Total Earnings     :     {this.state.todayTotalamount} INR</Text>
          </View>
          {this.renderList()}
        </ScrollView>

        <Modal
          open={this.state.open}
          style={{ alignItems: "center" }}
          closeOnTouchOutside={true}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
        >
          <View style={{ alignItems: "center" ,marginVertical:15}}>
          <DatePicker
        style={{width: 200}}
        date={this.state.date}
        mode="date"
        placeholder="select date"
        minDate= {mindate}
        maxDate={new Date()}
        format="DD/MM/YYYY"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        onDateChange={(date) => {this.setState({date: date})}}
      />
      <TouchableOpacity onPress={()=>{this.closeModal(),this.filterDate()}} style={{marginTop:20,padding:10,borderColor:'black',borderWidth:2}}>
          <Text style={{fontSize:16,fontFamily:'OpenSans-SemiBold',color:'black'}}>Confirm</Text>
      </TouchableOpacity>
          </View>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
        backgroundColor: theme
    },
    dateText:{
        fontSize: 14,
        fontFamily: 'OpenSans-SemiBold',
        color: 'black'
    },
    amountText:{
        fontSize: 18,
        fontFamily: 'OpenSans-SemiBold',
        color: 'black'
    }
})
