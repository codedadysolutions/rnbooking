import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView,FlatList,ActivityIndicator,TouchableOpacity,Alert, TextInput,ToastAndroid } from 'react-native'
import DetailCard from './src/DetailCard'
import Header from './src/Header'
import firebase from 'react-native-firebase'
import axios from 'react-native-axios'
import Modal from 'react-native-simple-modal'
import {messageAPI,senderid,bussinessname,bussinessphone,serviceTypology,theme} from './config'
// this.onPress(item.key), this.sendSms(item.phone,item.name,item.vehiclenumber)}

export default class WorkingList extends Component {

    state= {
        status:'completed',
        data:[],
        refresh:false,
        loader:true,
        open:false,
        amount:null
    }

async componentDidMount(){
   

    var db = firebase.firestore();
    var that =this

    db.collection("users").where("status", "==", "working")
    .get()
    .then(function(querySnapshot) {
        const data = [];
        // var i = 0
    
        querySnapshot.forEach(function(doc) {
            // console.warn(doc.id, " => ", doc.data());
            data.push({
                key: doc.id,
                name:doc.data().name,
                phone:doc.data().phone,
                place:doc.data().place,
                type:doc.data().type,
                modelnumber:doc.data().modelnumber,
                amount:doc.data().amount
              });
        });
       
        that.setState({
            data,
            loader:false,
            // amount:doc.data().amount
        })
        
    })
    .catch(function(error) {
        that.setState({
            loader:false
        })
        console.log("Error getting documents: ", error);
    });
    
}



onLongPressedDelete=(key)=>{
    Alert.alert(
        'Alert',
        'Are you sure want to delete this entry',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => this.removefromDb(key)},
        ],
        { cancelable: false }
      )
}

removefromDb =async()=>{
    var key = this.state.key
    var data =this.state.data
    var removeByAttr = function(arr, attr, value){
        var i = arr.length;
        while(i--){
           if( arr[i] 
               && arr[i].hasOwnProperty(attr) 
               && (arguments.length > 2 && arr[i][attr] === value ) ){ 
    
               arr.splice(i,1);
    
           }
        }
        return arr;
    }
    
        var list = removeByAttr(data, 'key', key);
        this.setState({data:list,refresh: !this.state.refresh})

        var db =firebase.firestore()

        db.collection("users").doc(key).delete().then(function() {
            console.warn("Document successfully deleted!");
        }).catch(function(error) {
            console.error("Error removing document: ", error);
        });
        
}

modalDidOpen = () => console.log("Modal did open.");

modalDidClose = () => {
  this.setState({ open: false });
  console.log("Modal did close.");
};


openModal = () => this.setState({ open: true });

closeModal = () => this.setState({ open: false });

alertForCompleteButton=()=>{
    this.openModal()
}

onPress = async()=>{

var key =this.state.key
var data =this.state.data
var removeByAttr = function(arr, attr, value){
    var i = arr.length;
    while(i--){
       if( arr[i] 
           && arr[i].hasOwnProperty(attr) 
           && (arguments.length > 2 && arr[i][attr] === value ) ){ 

           arr.splice(i,1);

       }
    }
    return arr;
}

    var list = removeByAttr(data, 'key', key);
    this.setState({data:list,refresh: !this.state.refresh})

    var db = firebase.firestore();
    db.collection("users").doc(key).update({
        status:'completed',
        amount:parseFloat(this.state.amount)
    });

   

}


sendSms=()=>{
    var that =this
    axios.get('http://api.msg91.com/api/sendhttp.php?country=91&sender='+senderid+'&route=4&mobiles='+91+this.state.phone+'&authkey='+messageAPI+'&message=Hello, '+this.state.name+' your mobile [' + this.state.modelnumber + '] '+serviceTypology+' completed. Amount Rs. '+ this.state.amount +'. Please collect your mobile. Thank you '+bussinessname+' : '+bussinessphone)
  .then(function (response) {
    // that.updateMessageDb()
    console.warn(response)
    console.warn(bussinessname)
  })
  .catch(function (error) {
    console.log(error);
  });
}


updateMessageDb =()=>{
    var db = firebase.firestore();
    var sfDocRef = db.collection("loginAcess").doc('detail')
    var that = this

    return db.runTransaction(function(transaction) {
      
        return transaction.get(sfDocRef).then(function(sfDoc) {
            if (!sfDoc.exists) {
                throw "Document does not exist!";
            }
              var newCount = sfDoc.data().messageCount - 1;
              transaction.update(sfDocRef, { messageCount: newCount });
          });

      }).then(function() {
     
        ToastAndroid.show('SMS send successfully!', ToastAndroid.SHORT);

      }).catch(function(error) {
          console.log("Transaction failed: ", error);
      });
}

renderCard =()=>{
    if(this.state.loader){
        return(
            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                <ActivityIndicator size={42} color="white"/>
            </View>
        )
    }else{
    return(
        <FlatList
        extraData={this.state.refresh}
        data={this.state.data}
        renderItem={({ item,index }) => (
            <TouchableOpacity onLongPress={()=>this.onLongPressedDelete(item.key)}>
                 <DetailCard id={index + 1} clientname = {item.name} phone = {item.phone} modelNo = {item.modelnumber} sType = {item.type} amount = {item.amount} status = {this.state.status} onPress={()=>{this.alertForCompleteButton(),this.setState({key:item.key,name:item.name,modelnumber:item.modelnumber,phone:item.phone,amount:item.amount.toString()})}}/>
            </TouchableOpacity>
        )
        }
    />
    )
    }
}

  render() {
      console.warn(this.state.amount)
    return (
      <View style={styles.mainContainer}>
        <Header title = 'Working List' onPresss = {()=>this.props.navigation.openDrawer()}/>
        {this.renderCard()}
        <Modal
          open={this.state.open}
          closeOnTouchOutside={true}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center",}}
        >
          <View style={{height: 200, justifyContent:'center'}}>
      {/* <TouchableOpacity onPress={this.closeModal} style={{marginTop:20,padding:10,borderColor:'grey',borderWidth:1}}>
          <Text>Confirm</Text>
      </TouchableOpacity> */}
      <View style={{flex:1, justifyContent: "center", alignItems: 'flex-start'}}>
      <Text style={{
              fontFamily: 'OpenSans-Bold',
              fontSize: 16,
              color:'black'
          }}>Please type the service cost</Text>
      </View>
      <View style={{flex:1}}>
      <TextInput style={{height:50, borderWidth: 2, paddingLeft:10}}
        onChangeText={(amount)=>{this.setState({amount: parseFloat(amount)})}}
        value={this.state.amount}
        keyboardType={"number-pad"}
      />
      </View>
      <TouchableOpacity onPress={()=>{this.onPress(), this.sendSms(),this.setState({open:false})}} style={{backgroundColor: "#101839", marginVertical:10, flex:.7, borderRadius: 100, justifyContent: "center", alignItems: 'center'}}>
          <Text style={{
              fontFamily: 'OpenSans-Bold',
              fontSize: 16,
              color:'white'
          }}>Send</Text>
      </TouchableOpacity>
          </View>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
        backgroundColor: theme
    },
    headLineView:{
        height: 50,
        borderWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    headlineText: {
        fontSize: 20,
        fontFamily: 'OpenSans-Bold'
    }
})

