import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, TouchableOpacity } from 'react-native'
import Header from './src/Header'
import DetailCard from './src/DetailCard'
import Icon from 'react-native-vector-icons/dist/Entypo'
import EarningsCard from './src/EarningsCard'
import {Calendar} from 'react-native-calendars';
import moment from 'moment'
import firebase from 'react-native-firebase'
import {minEarning, theme} from './config'


export default class Earnings extends Component {

    state ={
        totalAmount:0,
        totalAmountDay:0,
        date:''
    }




    async componentDidMount(){
        var db = firebase.firestore();
        var that =this
        var month = moment().format('MMMM YYYY')
    
        db.collection("users").where('month', '==', month).where("status", "==", "paid")
        .get()
        .then(function(querySnapshot) {

            var amount = that.state.totalAmount
    
            querySnapshot.forEach(function(doc) {
                  amount = amount + doc.data().amount
            });

            that.setState({
                totalAmount:amount,
            })
        })
        .catch(function(error) {
            console.log("Error getting documents: ", error);
        });  

        

        var day = moment().format('DD/MM/YYYY')
    

        db.collection("users").where('dateModified', '==', day).where("status", "==", "paid")
        .get()
        .then(function(querySnapshot) {

            var amount = that.state.totalAmountDay
    
            querySnapshot.forEach(function(doc) {
                  amount = amount + doc.data().amount
            });
            that.setState({
                totalAmountDay:amount,
            })
        })
        .catch(function(error) {
            console.log("Error getting documents: ", error);
        });

        this.setState({date:moment().format("YYYY-MM-DD")})

    }

    filterbyMonth =async(month)=>{
        var db = firebase.firestore();
        var that =this
        var month = moment(month).format('MMMM YYYY')
        // console.warn(month)
        this.setState({totalAmount:0,totalAmountDay:0})

        db.collection("users").where('month', '==', month).where("status", "==", "paid")
        .get()
        .then(function(querySnapshot) {
            const data = [];
            var amount = that.state.totalAmount
    
            querySnapshot.forEach(function(doc) {
             
                  amount = amount + doc.data().amount
            });
            that.setState({
                totalAmount:amount,
            })
        })
        .catch(function(error) {
            console.log("Error getting documents: ", error);
        });
    }


    getDayEarning =async(day)=>{
        var db = firebase.firestore();
        var that =this
        var day = moment(day).format('DD/MM/YYYY')
    
         this.setState({totalAmountDay:0})

        db.collection("users").where('dateModified', '==', day).where("status", "==", "paid")
        .get()
        .then(function(querySnapshot) {

            var amount = that.state.totalAmountDay
    
            querySnapshot.forEach(function(doc) {
                  amount = amount + doc.data().amount
            });
            that.setState({
                totalAmountDay:amount,
            })
        })
        .catch(function(error) {
            console.log("Error getting documents: ", error);
        });
    }

    render() {
     console.warn(this.state.date)
        return (
        <View style={styles.mainContainer}>
             <Header title = 'Earnings' onPresss = {()=>this.props.navigation.openDrawer()} />
        <ScrollView style={{flex:1}}>
            <Calendar
            style={styles.calendar}
            minDate={minEarning}
            markedDates={{
                [this.state.date]: {selected: true, marked: true, selectedColor: '#101839'},
              }}
            maxDate={new Date()}
            onMonthChange={(month) => {this.filterbyMonth(month.timestamp)}}
            onDayPress={(day) => {this.getDayEarning(day.timestamp),this.setState({date:day.dateString})}}
            />
            <View style={{height: 60, justifyContent:'center', backgroundColor: 'white', marginVertical:10, alignItems: 'center'}}>
                <Text style={[styles.dateText]}>Today : {this.state.totalAmountDay} INR</Text>
            </View>
            <View style={{height: 60, justifyContent:'center', backgroundColor: 'white', marginVertical: 10, alignItems: 'center'}}>
                <Text style={[styles.dateText]}>This Month : {this.state.totalAmount} INR</Text>
            </View>
          
            </ScrollView>
        </View>
      
        )
      }
    }
    
    const styles = StyleSheet.create({
        mainContainer:{
            flex:1,
            backgroundColor: theme
        },
        dateText:{
            fontSize: 16,
            fontFamily: 'OpenSans-SemiBold',
            color: 'black'
        },
        calendar:{
            marginVertical: 10
        }
    })
    
