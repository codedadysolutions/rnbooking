import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, TouchableOpacity,ActivityIndicator,ToastAndroid,FlatList } from 'react-native'
import { TextField } from 'react-native-material-textfield'
import Header from './src/Header'
import Icon from 'react-native-vector-icons/dist/Entypo'
import firebase from 'react-native-firebase'
import axios from 'react-native-axios'
import Modal from 'react-native-simple-modal'
import {messageAPI,senderid,bussinessname,bussinessphone,serviceTypologyR, dataService, theme} from './config'
// import {dataService} from './serviceConfig'

export default class Register extends Component {

    state = {
        phone: '',
        name: '',
        place: '',
        modelnumber: '',
        errPhone: '',
        errName:'',
        errModel: '',
        errPlace: '',
        status:'full',
        type: '',
        errType:'',
        amount:null,
        loader:false,
        open:false,
        // dataService:[
        //     {
        //         id:"1",
        //        name:"Interior + Exteroir",
        //        price:300
        
        //     },
        //     {
        //         id:"2",
        //        name:"Exteroir only",
        //        price:200
        
        //     },
        //     {
        //         id:"3",
        //         name:"Full",
        //         price:500
         
        //     }
        
        // ]
      };

      onSubmit = ()=>{
         this.setState({loader:true})
          if (this.state.name == '' ){
            this.setState({errName: 'Please enter the name',loader:false})
          }
          else if (this.state.type == ''){
            this.setState({errType: 'Please select service type',loader:false})
          }
          else if(this.state.phone == ''){
            this.setState({errPhone: 'Please enter the phone number',loader:false})
          }
          else if (this.state.phone.length != 10){
            this.setState({errPhone: 'Please enter a valid phone number',loader:false})
          }
          else if(this.state.place == ''){
            this.setState({errPlace: 'Please enter the place',loader:false})
          }
          else if (this.state.modelnumber == ''){
            this.setState({errModel: 'Please enter the model',loader:false})
          }
          else{
             this.pushtoDb(),
             this.sendSms()
           
          }
      }

      modalDidOpen = () => console.log("Modal did open.");

      modalDidClose = () => {
        this.setState({ open: false });
        console.log("Modal did close.");
      };
      
      
      openModal = () => this.setState({ open: true });
      
      closeModal = () => this.setState({ open: false });

      pushtoDb =()=>{
        var db = firebase.firestore();
        db.collection("users").add({
            name:this.state.name,
            phone:this.state.phone,
            place:this.state.place,
            status:'working',
            type:this.state.type,
            modelnumber:this.state.modelnumber,
            amount:this.state.amount,
            date:firebase.firestore.FieldValue.serverTimestamp()
        });
        
      }

      sendSms=()=>{
          var that = this
        axios.get('http://api.msg91.com/api/sendhttp.php?country=91&sender='+senderid+'&route=4&mobiles='+91+this.state.phone+'&authkey='+messageAPI+'&message=Hello, '+this.state.name+' your mobile [' +this.state.modelnumber+ '] has been recived for '+serviceTypologyR+'. You will recive a message after the service. Thank you '+bussinessname+ ' : '+bussinessphone)
        .then(function (response) {
          that.updateMessageDb()
        })
        .catch(function (error) {
          console.log(error);
        });
        this.setState({loader:false,phone:'',name:'',type:'',place:'',modelnumber:''},()=>{
            ToastAndroid.show('User added', ToastAndroid.SHORT);
        })
      }

      updateMessageDb =()=>{
        var db = firebase.firestore();
        var sfDocRef = db.collection("loginAcess").doc('detail')
        var that = this
    
        return db.runTransaction(function(transaction) {
          
            return transaction.get(sfDocRef).then(function(sfDoc) {
                if (!sfDoc.exists) {
                    throw "Document does not exist!";
                }
                  var newCount = sfDoc.data().messageCount - 1;
                  transaction.update(sfDocRef, { messageCount: newCount });
              });
    
          }).then(function() {
         
            ToastAndroid.show('SMS send successfully!', ToastAndroid.SHORT);
    
          }).catch(function(error) {
              console.log("Transaction failed: ", error);
          });
    }

    renderLoader =()=>{
        if(this.state.loader){
            return (
              <View style={styles.buttonView}>
                  <ActivityIndicator color="black" size={36} />
               </View>
            )
        }else{
            return(
            <TouchableOpacity onPress = {this.onSubmit} style={styles.buttonView}>
                 <Text style={styles.buttonText}>Done</Text>
             </TouchableOpacity>
            )
        }
    }
      
    renderList =()=>{
        return(
            <FlatList
            data={dataService}
            renderItem={({item}) => 
            (
                <View style={{flex:1}}>
                <TouchableOpacity style={{marginVertical:10, borderWidth: 2, flex:1, padding: 10, justifyContent:'center', alignItems:'center'}} onPress={()=>{this.setState({type:item.name, amount: item.price}), this.closeModal()}}>
                    <Text style={{fontFamily:'OpenSans-SemiBold', fontSize:16, color: 'black'}}>{item.name}</Text>
                </TouchableOpacity>
                </View>
            )
            }
             />
           
        )
    }
  render() {
    // console.warn(data)
    let { phone, name, place, modelnumber, type } = this.state;
    return (
      <View style={styles.mainContainer}>
        <Header title = 'Register' onPresss = {()=>this.props.navigation.openDrawer()}/>
      <ScrollView styles={{flex:1}}>
            {/* <View style={styles.registerView}>
                <Text style={styles.headLineText}>Register</Text>
            </View> */}

            <TouchableOpacity style={styles.textFieldView} onPress={this.openModal}>
                <TextField
                            label='Service Type'
                            value={type}
                            disabled={true}
                            textColor='white'
                            tintColor= 'white'
                            baseColor= 'white'
                            onChangeText={ (type) => this.setState({ type, errType: '' }) }
                            error = {this.state.errType}
                        />
            </TouchableOpacity>

            <View style={styles.textFieldView}>
                        <TextField
                        label='Name'
                        value={name}
                        textColor='white'
                        tintColor= 'white'
                        baseColor= 'white'
                        onChangeText={ (name) => this.setState({ name, errName: '' }) }
                        error = {this.state.errName}
                    />

            </View>
            <View style={styles.textFieldView}>
                <TextField
                            label='Phone'
                            value={phone}
                            keyboardType={"phone-pad"}
                            prefix= '+91'
                            textColor='white'
                            tintColor= 'white'
                            baseColor= 'white'
                            onChangeText={ (phone) => this.setState({ phone, errPhone:'' }) }
                            error = {this.state.errPhone}
                        />
            </View>
            <View style={styles.textFieldView}>
                <TextField
                            label='Place'
                            value={place}
                            textColor='white'
                            tintColor= 'white'
                            baseColor= 'white'
                            onChangeText={ (place) => this.setState({ place, errPlace:'' }) }
                            error = {this.state.errPlace}
                        />
            </View>
            <View style={styles.textFieldView}>
                <TextField
                            label='model No.'
                            value={modelnumber}
                            textColor='white'
                            tintColor= 'white'
                            baseColor= 'white'
                            autoCapitalize={'characters'}
                            keyboardType={"default"}
                            onChangeText={ (modelnumber) => this.setState({ modelnumber, errModel: '' }) }
                            error = {this.state.errModel}
                        />
            </View>

        </ScrollView>

         {this.renderLoader()}

         <Modal
          closeOnTouchOutside={true}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          open={this.state.open}
          style={{flex:1}}
        >
          <View style={{}}>
            {this.renderList()}
          </View>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
        backgroundColor: theme
    },
    registerView:{
        height: 70,
        borderWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',

    },
    textFieldView:{
        height: 50,
        borderWidth: 0,
        margin: 20,

    },
    buttonView:{
        // flex:.5,
        height: 60,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        marginTop: 30
    },
    headLineText:{
       fontSize: 22,
       fontFamily: 'OpenSans-Bold',
       color: 'white'
    },
    buttonText:{
        fontSize: 17,
        color: 'black',
        fontFamily: 'OpenSans-Bold'
    },
    iconView:{

    }
})


